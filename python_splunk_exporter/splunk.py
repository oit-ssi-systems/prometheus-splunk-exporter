import splunklib.results as results
import splunklib.client as client
from time import sleep, tzname
import dateparser
import logging


class api(object):
    """Splunk object
    """
    pass

    def __init__(self, *args, **kwargs):
        username = kwargs.get('username')
        password = kwargs.get('password')
        server = kwargs.get('server')

        self.service = client.connect(username=username,
                                      password=password,
                                      host=server)

    def search(self, *args, **kwargs):
        """Do a splunk search

        Parameters
        ----------
        query : str
          Splunk string to query
        earliest_time: str
          Earliest time to query, must be parseable with dateparser
        latest_time: str
          Latest time to query, must be parseable with dateparser
        return_count : int
          Number of results to return.  Defaults to 100
        """
        query = args[0]
        return_count = kwargs.get('return_count', 100)
        earliest_time = kwargs.get('earliest_time', '1 hour ago')
        latest_time = kwargs.get('latest_time', 'now')

        tz = tzname[1]
        tz_info = {'TIMEZONE': tz, 'TO_TIMEZONE': 'US/Eastern'}

        earliest_time_obj = dateparser.parse(earliest_time, settings=tz_info)
        latest_time_obj = dateparser.parse(latest_time, settings=tz_info)

        logging.debug("Searching between %s and %s" %
                      (earliest_time_obj, latest_time_obj))

        kwargs_normalsearch = {
            "exec_mode": "normal",
            'enable_lookups': True,
            'earliest_time': earliest_time_obj.isoformat(),
            'latest_time': latest_time_obj.isoformat(),
            "count": return_count
        }
        if query[0] != '|':
            real_query = "{}".format(query)
        else:
            real_query = query

        job = self.service.jobs.create("search %s" % real_query,
                                       **kwargs_normalsearch)
        while True:
            while not job.is_ready():
                pass
            stats = {
                "isDone": job["isDone"],
                "doneProgress": float(job["doneProgress"]) * 100,
                "scanCount": int(job["scanCount"]),
                "eventCount": int(job["eventCount"]),
                "resultCount": int(job["resultCount"])
            }

            if stats["isDone"] == "1":
                break
            sleep(1)

        return_items = results.ResultsReader(
            job.results(**kwargs_normalsearch))
        job.cancel()

        return return_items
