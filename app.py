#!/usr/bin/env python3
import sys
from python_splunk_exporter.splunk import api
from datetime import datetime
import time
import asyncio
import os
import logging
from prometheus_client import start_http_server, Gauge


async def main(loop):
    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))
    start_http_server(8080)
    username = get_required_env('SPLUNK_USERNAME')
    password = get_required_env('SPLUNK_PASSWORD')
    server = get_required_env('SPLUNK_SERVER')

    logging.debug("Timezone is currently set to: %s" % time.tzname[1])

    splunk = api(username=username, password=password, server=server)

    prometheus_metrics = {
        'splunk_per_index_thruput_kb':
        Gauge('splunk_per_index_thruput_kb', 'Per index thruput in kb',
              ['cluster', 'index', 'splunk_server']),
        'splunk_per_index_thruput_eps':
        Gauge('splunk_per_index_thruput_events', 'Per index thruput Events',
              ['cluster', 'index', 'splunk_server']),
        'splunk_search_concurrency_active_hist_searches':
        Gauge('splunk_search_concurrency_active_hist_searches',
              'Active historical searches', ['cluster', 'splunk_server']),
        'splunk_search_concurrency_active_realtime_searches':
        Gauge('splunk_search_concurrency_active_realtime_searches',
              'Active realtime searches', ['cluster', 'splunk_server']),
        'splunk_search_concurrency_current_queue_size':
        Gauge('splunk_search_concurrency_current_queue_size',
              'Current Queue size', ['cluster', 'splunk_server']),
        'splunk_metrics_collection_seconds':
        Gauge('splunk_metrics_collection_seconds',
              'Metrics collection time spent in seconds', ['cluster'])
    }

    while True:

        start = datetime.now()

        # Kbps thruput
        for search_key in [
                'active_hist_searches', 'active_realtime_searches',
                'current_queue_size'
        ]:
            logging.info("Searching for %s" % search_key)
            query = f"""
index="_internal"  group=search_concurrency
| stats sum({search_key}) by splunk_server
| rename sum({search_key}) as search_concurrency_{search_key}
"""
            results = splunk.search(query, earliest_time='5m')
            for x in results:
                print("Looking at %s" % x)
                labels = {}
                all_info = list(x.items())
                for k, v in all_info[0:-1]:
                    labels[k] = v

                # Sometimes we get a nul, skip it yo'
                if 'splunk_server' not in labels:
                    continue

                logging.info("%s - %s" % (labels, list(all_info[-1])))
                prometheus_metrics["splunk_search_concurrency_%s" %
                                   search_key].labels(
                                       server, labels['splunk_server']).set(
                                           all_info[-1][1])

        for search_key in ['kb', 'eps']:
            query = f"""
index=_internal component=Metrics per_index_thruput | rename series as index
| stats sum({search_key}) by index splunk_server
| rename sum({search_key}) as splunk_per_index_thruput_{search_key}
"""

            results = splunk.search(query, earliest_time='5m')
            for x in results:
                labels = {}
                all_info = list(x.items())
                for k, v in all_info[0:-1]:
                    labels[k] = v
                logging.info("%s - %s" % (labels, list(all_info[-1])))
                prometheus_metrics["splunk_per_index_thruput_%s" %
                                   search_key].labels(
                                       server, labels['index'],
                                       labels['splunk_server']).set(
                                           all_info[-1][1])

        end = datetime.now()

        collection_time = end - start
        prometheus_metrics['splunk_metrics_collection_seconds'].labels(
            server).set(collection_time.seconds)
        time.sleep(300)

    return 0


def get_required_env(env_name):
    """Look up and return an environmental variable, or fail if not found."""
    if env_name not in os.environ:
        logging.error(("Oops, looks like you haven't set %s, please do that"
                       " and then try running the script again\n") % env_name)
        sys.exit(2)
    else:
        return os.environ[env_name]


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))
    loop.close()
